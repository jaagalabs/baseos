# INSTALLATION INSTRUCTIONS

## Automated (Recommended)
~~~bash
sudo ./prepareSDCard.sh
~~~

## Manual

>Adapted from the [Alpine Wiki](https://wiki.alpinelinux.org/wiki/Raspberry_Pi)

1. Mount your SD card to your workstation
2. Use fdisk to create a FAT32 partition. The FAT32 partition type is called W95 FAT32 (LBA) and its ID is 0xC.
3. Mark the newly created partition as bootable and save
4. Mount the previously created partition
5. Extract the contents of sustain_os_base.zip to your FAT32 partition
6. Unmount the SD Card.
