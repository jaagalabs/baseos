#!/bin/bash

echo

function show_help {
    echo
    echo -e "This script prepares a 2GB SD card with Sustain OS for a specified device ID."
    echo
    echo -e "Ensure you have the following available: "
    echo
    echo -e "\tUnique device name: e.g. device 21"
    echo -e "\tSD card: e.g. /dev/sdc"
    echo -e "\tSFDisk config: e.g. sustain_2GB.sfdisk"
    echo -e "\tOS zip file: e.g. sustain_base_os.zip"
    echo
}

function cleanup {
    echo "Closing partitions"
    umount $ROOT_PART
    rm -rf $ROOT_MOUNT_DIR
}

function read_device_id {
	echo -n "Device name? (e.g. device21) "
    read DEVICEID
    echo
}

function read_device_token {
    echo -n "What is the access token for this device on thingsboard?: "
    read DEVICE_ACCESS_TOKEN
    echo
}

function read_sd_card {
    echo
	echo "Available disks:"
	lsblk | grep disk | grep -v sda | grep -v vda | awk '{print $1, $4}'
	echo
    echo -n "Which of these is the SD Card? (e.g. sdc) "
    read DISK_DEVICE
    DISK=/dev/$DISK_DEVICE
    echo
}

function read_customapps_git_repo {
    echo -n "Git repo with your custom applications? (e.g. git@bitbucket.org:jaagasustain/customapps.git) "
    read CUSTOMAPPS_GIT_REPO
    echo
}

function read_customapps_git_branch {
    echo -n "Which git branch should this device run? (e.g. master) "
    read CUSTOMAPPS_GIT_BRANCH
    echo
}

function echo_parameters {
    
    echo "Device ID: $DEVICEID"
    
    echo "SD Card: $DISK"
    
    echo "SFDisk Config File: $SFDISK_FILE"
    
    echo "OS Zip File: $OS_ZIP_FILE"

}

# Defaults - assumptions listed

# Currently only support 2GB cards
SFDISK_FILE="$PWD/sustain_2GB.sfdisk"
OS_ZIP_FILE="$PWD/sustain_base_os.zip"
ROOT_MOUNT_DIR="/tmp/sustain_root/"
# This needs to be on a partition that allows symlinks e.g. EXT4, not FAT32
TEMP_OVL_DIR="/tmp/ovl/"
BASEAPPS_GIT_REPO="git@bitbucket.org:jaagasustain/baseapps.git"
BASEAPPS_GIT_BRANCH="supervisor_agent"
S3_KEYS_BUCKET="s3://devmicrograce/"

while :; do
    case $1 in
        -h|-\?|--help)   # Call "showHelp" function to display a synopsis, then exit.
            show_help
            exit
            ;;
        --)              # End of all options.
            shift
            break
            ;;
        -?*)
            printf 'WARN: Unknown option (ignored): %s\n' "$1" >&2
            ;;
        *)               # Default case: If no more options then break out of the loop.
            break
    esac

    shift
done


# Ask for user inputs
read_device_id
read_device_token
read_sd_card

read_customapps_git_repo
if [[ $CUSTOMAPPS_GIT_REPO != "" ]]; then
    read_customapps_git_branch
fi


if [[ $DEVICEID == "" ]] ; then

    echo
    
    echo "Please specify device ID."
    
    echo

    show_help
    
    exit 1

fi

if [[ ! -e "$DISK" ]]; then

    echo
    
    echo "Is the path correct? $DISK"
    
    echo

    show_help
    
    exit 1

fi


if [[ -f "$SFDISK_FILE" ]]; then

    echo "sfdisk config found - will use it to create partitions."

else
    
    echo -n "sfdisk config not found in $PWD. Please enter path to $SFDISK_FILE: "
    
    read SFDISK_FILE
    
    if [[ ! -f "$SFDISK_FILE" ]]; then
    
        echo "Can not continue without $SFDISK_FILE. Exiting."
        
        exit 2
    
    fi

fi

# Clean up existing mounts, if any

if [[ $DISK = "/dev/mmcblk0" ]]; then
	ROOT_PART=$DISK"p1"
else
	ROOT_PART=$DISK"1"
fi

cleanup

if [[ -e "$DISK" ]]; then
    # Let's be nice and confirm before overwriting a disk
    echo
    
    echo "Using the following settings: "
    
    echo_parameters
    
    echo
    
    echo -n "Are you sure you want to continue? (y/n): "
    read response
    
    if [[ $response == "y" ]]; then
        echo "OK, preparing $DISK for $DEVICE"

        # Create partitions on pen drive using existing appropriate sfdisk config
        sfdisk --no-reread "$DISK" < $SFDISK_FILE

        if [[ $? -ne 0 ]]; then
            echo "sfdisk failed to partition the disk. Cannot continue - check for reported errors."
            cleanup
            exit 3
        fi
    else
        echo "OK, aborting!"
        cleanup
        exit 3
    fi
else
    echo "Disk not found. Are you sure you specified the right path?"
    cleanup
    exit 3
fi
echo

sleep 3

sync

mkfs.vfat "$ROOT_PART"

if [[ $? -ne 0 ]]; then
    echo "Failed to create filesystem on $ROOT_PART. Cannot continue - please check for error messages."
    cleanup
    exit 4
fi

mkdir "$ROOT_MOUNT_DIR"
mount -t vfat "$ROOT_PART" "$ROOT_MOUNT_DIR"

if [[ $? -ne 0 ]]; then
    echo "Failed to mount $ROOT_PART. Cannot continue - please check for error messages."
    cleanup
    exit 4
fi

echo "Copying OS..."
unzip "$OS_ZIP_FILE" -d "$ROOT_MOUNT_DIR"

# Update hostname and copy other essential files
# Due to the way Alpine overlays work, we have to unpack, edit and repack
mkdir "$TEMP_OVL_DIR"
tar xhf "$ROOT_MOUNT_DIR/rpi.apkovl.tar.gz" -C "$TEMP_OVL_DIR/"
echo "Writing hostname: $DEVICEID - "
echo "$DEVICEID" > "$TEMP_OVL_DIR/etc/hostname"
sed -i "1 s/^.*$/127.0.0.1 localhost $DEVICEID/" "$TEMP_OVL_DIR/etc/hosts"
echo "Done."

# echo
# echo "IMPORTANT"
# echo
# echo "Creating device specific ssh key. Be sure to back up the private key. You will not be able to ssh without this key!"
# echo
# ssh-keygen -f "$DEVICEID" -N ""
# cat "$DEVICEID.pub" > "$TEMP_OVL_DIR/home/sustain/.ssh/authorized_keys"
# aws s3 cp "$DEVICEID" $S3_KEYS_BUCKET
# aws s3 cp "$DEVICEID.pub" $S3_KEYS_BUCKET
echo "Copying device ssh key and storing bitbucket server signatures - "

mkdir -p "$TEMP_OVL_DIR/home/sustain/.ssh/"
mkdir -p "$TEMP_OVL_DIR/root/.ssh/"
cat id_rsa.pub > "$TEMP_OVL_DIR/home/sustain/.ssh/authorized_keys"
cp id_rsa "$TEMP_OVL_DIR/home/sustain/.ssh/id_rsa"
cp id_rsa.pub "$TEMP_OVL_DIR/home/sustain/.ssh/id_rsa.pub"
cp id_rsa "$TEMP_OVL_DIR/root/.ssh/id_rsa"
cp id_rsa.pub "$TEMP_OVL_DIR/root/.ssh/id_rsa.pub"
chmod 400 "$TEMP_OVL_DIR/home/sustain/.ssh/id_rsa"
chmod 400 "$TEMP_OVL_DIR/root/.ssh/id_rsa"
ssh-keyscan bitbucket.org >> "$TEMP_OVL_DIR/home/sustain/.ssh/known_hosts"
ssh-keyscan bitbucket.org >> "$TEMP_OVL_DIR/root/.ssh/known_hosts"
echo "Done."

# Update and deploy base apps
echo "Updating base apps including agent -"
echo
mkdir -p "$TEMP_OVL_DIR/home/sustain/apps/base"
git -C "$TEMP_OVL_DIR/home/sustain/git/baseapps" fetch
git -C "$TEMP_OVL_DIR/home/sustain/git/baseapps" checkout $BASEAPPS_GIT_BRANCH
git -C "$TEMP_OVL_DIR/home/sustain/git/baseapps" pull
git -C "$TEMP_OVL_DIR/home/sustain/git/baseapps" archive --format=tar HEAD | (cd "$TEMP_OVL_DIR/home/sustain/apps/" && tar xf -)
echo "Done."

# Set up credentials for Thingsboard
echo "Setting up thingsboard credentials - "

cp thingsboard.json "$TEMP_OVL_DIR/home/sustain/.credentials/thingsboard.json"
sed -i "s/DEVICE_ACCESS_TOKEN/$DEVICE_ACCESS_TOKEN/g" "$TEMP_OVL_DIR/home/sustain/.credentials/thingsboard.json"
echo "Done."

# Get and deploy custom apps

if [[ $CUSTOMAPPS_GIT_REPO != "" ]]; then
    echo "Downloading and deploying your custom apps - "
    echo
    mkdir -p "$TEMP_OVL_DIR/home/sustain/apps/custom"
    git clone $CUSTOMAPPS_GIT_REPO "$TEMP_OVL_DIR/home/sustain/git/customapps"
    git -C "$TEMP_OVL_DIR/home/sustain/git/customapps" checkout $CUSTOMAPPS_GIT_BRANCH
    git -C "$TEMP_OVL_DIR/home/sustain/git/customapps" archive --format=tar HEAD | (cd "$TEMP_OVL_DIR/home/sustain/apps/" && tar xf -)
    echo "Done."
fi

chown -R 1000:1000 "$TEMP_OVL_DIR/home/sustain/"

mkdir -p "$TEMP_OVL_DIR/usr/bin"
cp logstash_notifier "$TEMP_OVL_DIR/usr/bin/logstash_notifier"

cp supervisord.conf "$TEMP_OVL_DIR/etc/supervisord.conf"

tar czf "$ROOT_MOUNT_DIR/rpi.apkovl.tar.gz" -C "$TEMP_OVL_DIR/" .

rm -rf "$TEMP_OVL_DIR"

# Close everything to avoid corruption
cleanup

echo "Completed preparing the device."
